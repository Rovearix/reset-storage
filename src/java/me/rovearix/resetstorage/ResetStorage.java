package me.rovearix.resetstorage;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.plugin.java.JavaPlugin;

public class ResetStorage extends JavaPlugin implements CommandExecutor, Listener {

    //Configurable Variables
    public static String invTitle = "'s Saved Inventory!";
    public static int invSlots;
    public static boolean saveNBTData;

    //Enable Method
    @Override
    public void onEnable() {
        loadDriver();
        this.saveResource("ResetData.db", false);
        this.saveDefaultConfig();
        saveNBTData = this.getConfig().getBoolean("saveNBT");
        invSlots = this.getConfig().getInt("slots");
        getServer().getPluginManager().registerEvents(new ResetStorage(), this);
        Bukkit.getLogger().info("ResetStorage Enabled");
    }

    //Initializes the database driver
    public void loadDriver() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //Command Handler
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        //Player that is executing the command
        Player player = sender instanceof Player ? (Player) sender : null;

        //Stops non players from running the command
        if (player == null)
            return false;

        //Checks for the command name
        if (cmd.getName().equalsIgnoreCase("resetinv")) {

            //Checks for permissions
            if (player.isOp() || player.hasPermission("ResetStorage.user")) {
                player.sendMessage("Opening your saved inventory");

                //Opens the reset inventory
                player.openInventory(DBManager.getInventory(player));
                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("restrict")) {

            //Checks for permissions
            if (player.isOp() || player.hasPermission("ResetStorage.admin")) {

                //Checks for empty player hand
                if (player.getItemInHand() == null || player.getItemInHand().getType().equals(Material.AIR)) {
                    player.sendRawMessage("You are not holding anything in your hand.");
                    return true;
                }

                //Toggles the item restriction status
                if (!DBManager.check(player.getItemInHand().getType().toString())) {
                    if (DBManager.restrict(player.getItemInHand().getType().toString()))
                        player.sendRawMessage("Item has been successfully restricted.");
                    else
                        player.sendRawMessage("Item not has been successfully restricted!");
                } else {
                    if (DBManager.unrestrict(player.getItemInHand().getType().toString()))
                        player.sendRawMessage("Item has been successfully removed from the restricted items list.");
                    else
                        player.sendRawMessage("Item not has been successfully removed from the restricted list!");
                }
                return true;
            }
        } else if (cmd.getName().equalsIgnoreCase("check")) {

            //Checks for permissions
            if (player.isOp() || player.hasPermission("ResetStorage.admin")) {

                //Checks for empty player hand
                if (player.getItemInHand() == null || player.getItemInHand().getType().equals(Material.AIR)) {
                    player.sendRawMessage("You are not holding anything in your hand.");
                    return true;
                }

                //Checks for the item on the register
                if (DBManager.check(player.getItemInHand().getType().toString()))
                    player.sendRawMessage("This item is restricted!");
                else
                    player.sendRawMessage("This item is not restricted.");
                return true;
            }
        }

        //Returns false if not used
        return false;
    }

    //Blacklists items from entering the reset inventory
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().getTitle().equals(invTitle))
            if (DBManager.check(event.getWhoClicked().getItemOnCursor().getType().toString()))
                event.setCancelled(true);
    }

    //Updates the closed inventory
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        if (event.getInventory().getTitle().equals(invTitle))
            DBManager.update((Player) event.getPlayer(), event.getInventory());
    }
}