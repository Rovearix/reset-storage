package me.rovearix.resetstorage;

import com.comphenix.protocol.utility.StreamSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.*;

public class DBManager {

    //Database connection location
    public static final String DATABASE = "jdbc:sqlite:" + System.getProperty("user.dir") + "\\plugins\\ResetStorage\\ResetData.db";

    //Pulls the storage map from memory
    public static Inventory getInventory(Player player) {

        //Inventory to hold the players data
        Inventory storage = Bukkit.createInventory(player, ResetStorage.invSlots, ResetStorage.invTitle);
        try {

            //Opens the database connection
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();

            //Checks for an inventory in the database
            boolean newInventory = true;
            ResultSet resultSet = statement.executeQuery("SELECT Storage FROM UserData WHERE Username='" + player.getName() + "'");
            while (resultSet.next()) {
                newInventory = false;
                String encodedInventory = resultSet.getString(1);

                //Checks which type of inventory to decode
                if (encodedInventory.charAt(0) == 'A') {

                    //Converts the input string
                    String[] items = encodedInventory.substring(1).split(";");

                    //Reads the serialized inventory
                    for (int i = 0; i < items.length; ++i)
                        if (!items[i].equals("") && !check(items[i]))
                            storage.setItem(i, StreamSerializer.getDefault().deserializeItemStack(items[i]));
                } else {

                    //Checking for broken storage
                    if (encodedInventory.charAt(0) != 'B')
                        encodedInventory = 'B' + encodedInventory;

                    //Input Streams to convert the string
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(encodedInventory.substring(1)));
                    BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

                    //Reads the serialized inventory
                    for (int i = 0; i < ResetStorage.invSlots; i++) {

                        //Gets the item from the input stream
                        ItemStack item = (ItemStack) dataInput.readObject();

                        //Removed the item if banned
                        if (check(item.getType().toString()))
                            storage.setItem(i, item);
                    }
                }
            }

            //Adds the record to the database
            if (newInventory)
                add(player.getName(), storage);

            //Closes the database connection
            resultSet.close();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Returns the inventory
        return storage;
    }

    //Converts inventory to base64
    public static String convertInventory(Inventory inventory) throws IOException {

        //Stores the inventory with or without NBT Data
        if (ResetStorage.saveNBTData) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < inventory.getSize(); ++i) {
                if (i > 0)
                    stringBuilder.append(";");
                if (inventory.getItem(i) != null && inventory.getItem(i).getType() != Material.AIR)
                    stringBuilder.append(StreamSerializer.getDefault().serializeItemStack(inventory.getItem(i)));
            }
            return "A" + stringBuilder.toString();
        } else {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            for (int i = 0; i < inventory.getSize(); i++)
                dataOutput.writeObject(inventory.getItem(i));
            dataOutput.close();
            return "B" + Base64Coder.encodeLines(outputStream.toByteArray());
        }
    }

    //Adds a new player's storage to the database
    public static void add(String username, Inventory storage) {
        try {
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();
            statement.executeUpdate("INSERT INTO UserData (Username, Storage) VALUES('" + username + "', '" + convertInventory(storage) + "')");
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Saves the inventory data
    public static void update(Player player, Inventory storage) {

        //Remove restricted items
        for (int i = 0; i < storage.getSize(); i++)

            //Checks the item on the restricted list
            if (storage.getItem(i) != null && check(storage.getItem(i).getType().toString())) {

                //Looks for an empty space in the inventory to return the restricted item to
                for (int j = 0; j < player.getInventory().getSize(); j++)
                    if (player.getInventory().getItem(j) == null || player.getInventory().getItem(j).getType() == Material.AIR) {

                        //Returns the item back to the user
                        player.getInventory().setItem(j, storage.getItem(i));

                        //Removes the item from the reset storage
                        storage.setItem(i, new ItemStack(Material.AIR));

                        //Breaks out of the loop
                        break;
                    }

                //Warns the player about the use of restricted items
                player.sendRawMessage("Item " + storage.getItem(i).getType().toString() + " is not allowed in reset inventory!");
            }

        //Save modified inventory
        try {
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();
            statement.executeUpdate("UPDATE UserData set Storage= '" + convertInventory(storage) + "' WHERE Username='" + player.getName() + "'");
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Adds a restriction to the list
    public static boolean restrict(String itemID) {
        try {
            if (check(itemID))
                return false;
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();
            statement.executeUpdate("INSERT INTO Restricted (Item) " + "VALUES('" + itemID + "')");
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //Adds a restriction to the list
    public static boolean unrestrict(String itemID) {
        try {
            if (!check(itemID))
                return false;
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();
            statement.executeUpdate("DELETE FROM Restricted WHERE Item='" + itemID + "'");
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    //Checks the list for a restriction
    public static boolean check(String itemID) {
        boolean check = false;
        try {
            Connection dbConn = DriverManager.getConnection(DATABASE);
            Statement statement = dbConn.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Restricted");
            while (resultSet.next())
                if (resultSet.getString(1).equals(itemID))
                    check = true;
            resultSet.close();
            statement.close();
            dbConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }
}
